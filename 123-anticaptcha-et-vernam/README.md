# 123-anticaptcha et vernam

## Besoins pour l'attaque

* Un navigateur récent possédant une console
* Un bash pour faciliter la vie

## Principe et fonctionnement de l'attaque

Le développeur à fait un vernam avec un nonce => Donc les clés vont se répéter.

__Idéee :__ On va répéter beaucoup de fois le même chiffrement  pour essayer de trouver le nombre de clé

On lance 100 fois le chiffrement du même message de base (lancer le fichier _JS_ ci joint (_fichiersPourSolution/js.js_) dans la console de votre navigateur en modifiant la string s qui est le message de base)

Une fois que le script à fini de tourner, copier tout le contenu de la console dans un fichier.

### Sortir les chiffrés de tout ce bordel
Sur votre ligne de commandes, tapez :

```bash
cat nomFichierBordel | grep Chiffré > nomFichierDeSortie
```

Maintenant pour voire le nombre de chiffrés différents présents tapez : 
````bash
sort -u nomFichierDeSortie | cat -n
````

Voici le résultat pour le message de base _e:hz hsciavy rwx rjyt eaamgigs wqmegfaix fum xejvuwfinswyzawmldlk. fjgg fjyf_ :

```console
1  Chiffré = a:w:mr fkhafnd jbp wbdl jsfelalk birwlxfac xze cwonzokaskbqesbeqvqc. kbly kbdx debugger eval code:77:3
2  Chiffré = b:x:du etebwqc syq necu gtwhkjil slqfiywdb gwf tznwwpbdrtyrvvannwhf. jkiz becg debugger eval code:77:3
3  Chiffré = c:y:ux dcbcntb bvr ehbd dunkjsfm jopofznga ptg kcmftqsgqcvsmyzwkxyi. itfa shbp debugger eval code:77:3
4  Chiffré = d:z:la clydewa kss vkam avenibcn aroxcaejz yqh bfloqrjjplstdbyfhypl. hccb jkay debugger eval code:77:3
5  Chiffré = e:a:cd buvevzz tpt mnzv xwvqhkzo rungzbvmy hni sikxnsamoupuuexoezgo. glzc anzh debugger eval code:77:3
6  Chiffré = f:b:tg adsfmcy cmu dqye uxmtgtwp ixmpwcmpx qkj jljgktrpndmvlhwxbaxr. fuwd rqyq debugger eval code:77:3
7  Chiffré = g:c:kj zmpgdfx ljv utxn rydwfctq zalytddsw zhk aoiphuismmjwckvgybou. edte itxz debugger eval code:77:3
8  Chiffré = h:d:bm yvmhuiw ugw lwww ozuzelqr qdkhqeuvv iel rrhyevzvlvgxtnupvcfx. dmqf zwwi debugger eval code:77:3
9  Chiffré = i:e:sp xejillv ddx czvf lalcduns hgjqnflyu rbm iughbwqykedykqtysdwa. cvng qzvr debugger eval code:77:3
10  Chiffré = j:f:js wngjcou may tcuo ibcfcdkt yjizkgcbt ayn zxfqyxhbjnazbtshpend. bekh hcua debugger eval code:77:3
11  Chiffré = k:g:av vwdktrt vxz kftx fctibmhu pmhihhtes jvo qaezvyyeiwxaswrqmfeg. anhi yftj debugger eval code:77:3
12  Chiffré = l:h:ry ufalkus eua bisg cdklavev gpgreikhr ssp hddiszphhfubjzqzjgvj. zwej piss debugger eval code:77:3
13  Chiffré = m:i:ib toxmbxr nrb slrp zebozebw xsfabjbkq bpq ygcrpagkgorcacpighmm. yfbk glrb debugger eval code:77:3
```

#### 2 choses apparaissent
* Que 13 clés différentes
* Bizarre, un _:_ en plus dans le chiffré

### Essayons de faire pareil avec que des a
__Nouveau message :__

```
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
```

Cf. manip précédente et on se retrouve avec ça : 
```
1  Chiffré = a:sfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsfsf debugger eval code:77:3
2  Chiffré = b:twvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwve debugger eval code:77:3
3  Chiffré = c:unydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunyd debugger eval code:77:3
4  Chiffré = d:vebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebc debugger eval code:77:3
5  Chiffré = e:wvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwveb debugger eval code:77:3
6  Chiffré = f:xmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmha debugger eval code:77:3
7  Chiffré = g:ydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkz debugger eval code:77:3
8  Chiffré = h:zunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzuny debugger eval code:77:3
9  Chiffré = i:alqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqxmhalqx debugger eval code:77:3
10  Chiffré = j:bctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctw debugger eval code:77:3
11  Chiffré = k:ctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwv debugger eval code:77:3
12  Chiffré = l:dkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzunydkzu debugger eval code:77:3
13  Chiffré = m:ebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebctwvebct debugger eval code:77:3
```

#### Plusieurs choses :
* L'oracle rajoute bien une lettre et deux points mais en fait cette lettre identifie la clé correspondante !!!! => indice commence par e => __chiffré avec la clé 5__
* $(A+B) \   mod \  26 = (A +(B \  mod \ 26))\  mod 26 \ - \forall A \in [0, 26], B \in [0, 255]$ Donc _a_ + n’importe quelle nombre donne une autre lettre qui correspond à l'octet de la clé si on numérote l'alphabet de 0 à 26
* On se rend compte que sur chaque ligne, le même texte est répété tous les 6 caractères => __VIGENERE => pour nous la clé du Vigenère est :__  __**wvebct**__  

### Déchiffrer du Vigenère
* Faire des soustractions modulaire
* Utiliser une calculatrice de Vigenère comme [DCODE](https://www.dcode.fr/chiffre-vigenere)
* Utiliser une table de correspondance :

![table correspondance](https://www.dcode.fr/tools/vigenere/images/table.png "table de correspondance - Vigenère")
## Annexe : JS dans fichier "js.js"
```js
// ************************************************************************** //
// Fonction qui permet de remplacer un caractère à la position voulue
// @index		entier	endroit ou remplacer le caractère (commence à 0)
// @replacement	char	caractère qui va remplacer l'ancien
// RETURN la chaine modifiée
String.prototype.replaceAt = function( index, replacement ) {
    // Si la position voulue est supérieur à la taille de la chaine alors on ignore la modification
	if( index > this.length - 1 ) 
		return this;
    return this.substr( 0, index ) + replacement + this.substr( index + 1 );
}
// ************************************************************************** //


// ************************************************************************** //
// Promesse qui renvoie le resultat du captcha en attendant 1 seconde afin que le serveur est le temps d'envoyé le captcha
// RETURN une promesse contenant la réponse au captacha
const recupereReponseChallenge = () => new Promise( function( resolve, reject ) {
  setTimeout( function() {
    // On récupère le challenge
    let chal = document.querySelector( '#thechallenge' ).innerText;

    // On transforme la base 13 en base 10
    let responseCaptcha = parseInt( chal, 13 );
	//console.log("chal = " + chal + " et resp : " + responseCaptcha); 
    
    // Renvoie Promesse qui est le rés
    resolve( responseCaptcha );
  }, 1000 );
});
// ************************************************************************** //


// ************************************************************************** //
// Promesse qui renvoie le chiffré en attendant 1 seconde afin que le serveur est le temps de l'envoyé
// RETURN une promesse contenant le chiffré
const recupereChiffre = () => new Promise( function( resolve, reject ) {
  setTimeout( function() {
    // On récupère le challenge
    let chiffre = document.querySelector( '#thechiffre' ).innerText;

	//console.log("chiffré = " + chiffre ); 
    
    // Renvoie Promesse qui est le rés
    resolve( chiffre );
  }, 1000 );
});
// ************************************************************************** //



// ************************************************************************** //
// Promesse qui envoie le message claire
// @charTest	String	chaine que l'on envoie
const envoieClair = ( charTest ) => new Promise( function( resolve, reject ) {
  setTimeout( function() {

    // On met la valeur du formulaire à ce que l'on veut tester
    document.querySelector('#theplaintext').value = charTest;
    
    // On soumet le formumaire avec du jQuery sinon le handle jQuery ne gérera pas la soumission via js
     $('#plaintext').submit();//document.querySelector('#plaintext').submit();
    
    // Renvoie Promesse qui est le rés
    resolve();
  }, 1000 );
});
// ************************************************************************** //


// ************************************************************************** //
// -- ASYNC --
// Fonction qui soummet un texte et log le chiffré
// @chaine		String	chaine sur laquelle on travaile
// @index		entier	endroit que l'on regarde actuellement dans la chaine
// @replacement	entier	valeur décimal correspondant au caractère qui va remplacer le caractère que l'on regarde actuellement
async function soumetEtReponse( chaine, index, replacement ) {
    
	// On remplce dans la chaine par le caractère que l'on teste
    let charTest = chaine.replaceAt( index, String.fromCharCode( replacement ) );
    console.log( "On teste : " + charTest + ".");
	
	await envoieClair( charTest );
        
    let responseCaptcha;
	// On attend la réponse du captcha
    await recupereReponseChallenge().then( function( value ) {
      responseCaptcha = value;
    });
    
    //console.log("Le captcha en base 10 est : " + responseCaptcha  );
    
    // On me le res dans le formulaire
    document.querySelector('#theresponse').value = responseCaptcha;
    
    // On soumet le formumaire
    $('#challenge').submit();
	
	let messageChiffre;
	// On attend le message chiffré
    await recupereChiffre().then( function( value ) {
      messageChiffre = value;
    });
	
	//console.log( "Chiffré = " + messageChiffre );
	
	document.querySelector('.resetLink').click();
	
	return messageChiffre;
  
}
// ************************************************************************** //


// ************************************************************************** //
// -- ASYNC --
// Fonction qui réalise le brute force
async function bruteForce() {
	//let s = "e:hz hsciavy rwx rjyt eaamgigs wqmegfaix fum xejvuwfinswyzawmldlk. fjgg fjyf";
	let s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	let sLenght = s.length;

	let res = "";

	// Pour tous les caractères de la chaine que l'on a
	for( let i = 0; i < 100; i++ ) {
		
		
			
				let a = await soumetEtReponse( s, 5, 97 );
				console.log( "Chiffré = " + a );
				
	
		
	}
}
// ************************************************************************** //



// ************************************************************************** //
//                                  MAIN                                      //
// ************************************************************************** //
bruteForce();
// ************************************************************************** //
```
