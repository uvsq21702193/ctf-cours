// ************************************************************************** //
// Fonction qui permet de remplacer un caractère à la position voulue
// @index		entier	endroit ou remplacer le caractère (commence à 0)
// @replacement	char	caractère qui va remplacer l'ancien
// RETURN la chaine modifiée
String.prototype.replaceAt = function( index, replacement ) {
    // Si la position voulue est supérieur à la taille de la chaine alors on ignore la modification
	if( index > this.length - 1 ) 
		return this;
    return this.substr( 0, index ) + replacement + this.substr( index + 1 );
}
// ************************************************************************** //


// ************************************************************************** //
// Promesse qui renvoie le resultat du captcha en attendant 1 seconde afin que le serveur est le temps d'envoyé le captcha
// RETURN une promesse contenant la réponse au captacha
const recupereReponseChallenge = () => new Promise( function( resolve, reject ) {
  setTimeout( function() {
    // On récupère le challenge
    let chal = document.querySelector( '#thechallenge' ).innerText;

    // On transforme la base 13 en base 10
    let responseCaptcha = parseInt( chal, 13 );
	//console.log("chal = " + chal + " et resp : " + responseCaptcha); 
    
    // Renvoie Promesse qui est le rés
    resolve( responseCaptcha );
  }, 1000 );
});
// ************************************************************************** //


// ************************************************************************** //
// Promesse qui renvoie le chiffré en attendant 1 seconde afin que le serveur est le temps de l'envoyé
// RETURN une promesse contenant le chiffré
const recupereChiffre = () => new Promise( function( resolve, reject ) {
  setTimeout( function() {
    // On récupère le challenge
    let chiffre = document.querySelector( '#thechiffre' ).innerText;

	//console.log("chiffré = " + chiffre ); 
    
    // Renvoie Promesse qui est le rés
    resolve( chiffre );
  }, 1000 );
});
// ************************************************************************** //



// ************************************************************************** //
// Promesse qui envoie le message claire
// @charTest	String	chaine que l'on envoie
const envoieClair = ( charTest ) => new Promise( function( resolve, reject ) {
  setTimeout( function() {

    // On met la valeur du formulaire à ce que l'on veut tester
    document.querySelector('#theplaintext').value = charTest;
    
    // On soumet le formumaire avec du jQuery sinon le handle jQuery ne gérera pas la soumission via js
     $('#plaintext').submit();//document.querySelector('#plaintext').submit();
    
    // Renvoie Promesse qui est le rés
    resolve();
  }, 1000 );
});
// ************************************************************************** //


// ************************************************************************** //
// -- ASYNC --
// Fonction qui soummet un texte et log le chiffré
// @chaine		String	chaine sur laquelle on travaile
// @index		entier	endroit que l'on regarde actuellement dans la chaine
// @replacement	entier	valeur décimal correspondant au caractère qui va remplacer le caractère que l'on regarde actuellement
async function soumetEtReponse( chaine, index, replacement ) {
    
	// On remplce dans la chaine par le caractère que l'on teste
    let charTest = chaine.replaceAt( index, String.fromCharCode( replacement ) );
    console.log( "On teste : " + charTest + ".");
	
	await envoieClair( charTest );
        
    let responseCaptcha;
	// On attend la réponse du captcha
    await recupereReponseChallenge().then( function( value ) {
      responseCaptcha = value;
    });
    
    //console.log("Le captcha en base 10 est : " + responseCaptcha  );
    
    // On me le res dans le formulaire
    document.querySelector('#theresponse').value = responseCaptcha;
    
    // On soumet le formumaire
    $('#challenge').submit();
	
	let messageChiffre;
	// On attend le message chiffré
    await recupereChiffre().then( function( value ) {
      messageChiffre = value;
    });
	
	//console.log( "Chiffré = " + messageChiffre );
	
	document.querySelector('.resetLink').click();
	
	return messageChiffre;
  
}
// ************************************************************************** //


// ************************************************************************** //
// -- ASYNC --
// Fonction qui réalise le brute force
async function bruteForce() {
	//let s = "e:hz hsciavy rwx rjyt eaamgigs wqmegfaix fum xejvuwfinswyzawmldlk. fjgg fjyf";
	let s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	let sLenght = s.length;

	let res = "";

	// Pour tous les caractères de la chaine que l'on a
	for( let i = 0; i < 100; i++ ) {
		
		
			
				let a = await soumetEtReponse( s, 5, 97 );
				console.log( "Chiffré = " + a );
				
	
		
	}
}
// ************************************************************************** //



// ************************************************************************** //
//                                  MAIN                                      //
// ************************************************************************** //
bruteForce();
// ************************************************************************** //