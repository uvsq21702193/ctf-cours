#!/bin/bash

#Le premier message est 001
cnt=1

#Tant que le compteur est inférieur à 999 (le numéro du dernier message)
while [ $cnt -le 999 ];
do

        #Les 3 if c'est pour le nombre de zeros dans le nom du message ("message-00$cnt.txt") qui change.
        #Pour chaque message, on lance la commande openssl, qui va vérifier si le message correspond bien à la signature qu'on a.
        #Cela va échouer 998 fois, et réussir 1, et ce sera alors le bon message.
        if [ $cnt -le 9 ]
        then
                cmd_output=$(openssl dgst -sha256 -verify alice.pub -signature alice-ecdsa-sha256-signature.bin message-00$cnt.txt)  
        elif [ $cnt -gt 99 ]
        then
                cmd_output=$(openssl dgst -sha256 -verify alice.pub -signature alice-ecdsa-sha256-signature.bin message-$cnt.txt) 
        elif [ $cnt -gt 9 ];
        then
                cmd_output=$(openssl dgst -sha256 -verify alice.pub -signature alice-ecdsa-sha256-signature.bin message-0$cnt.txt)
        fi

        #On incrémente le compteur
        cnt=$(($cnt + 1))
        
        #Si l'output de la commande n'est pas "Verification Failure", on a bien trouvé le bon message ! :)
        if [ "$cmd_output" != "Verification Failure" ];
        	then
                echo $(($cnt - 1))
        	echo $cmd_output
        	echo "FOUND"
        	exit 1
        fi
done
